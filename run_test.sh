echo "Sequentially"
import -window root -resize 1600x900 -delay 5000 /home/sopel/Pictures/screenshotMPI_1.png
mpiexec -n 1 python3 main.py 
mpiexec -n 1 python3 main.py 
mpiexec -n 1 python3 main.py 
echo "\nMPI = 2"
import -window root -resize 1600x900 -delay 5000 /home/sopel/Pictures/screenshotMPI_2.png
mpiexec -n 2 python3 main.py 
mpiexec -n 2 python3 main.py 
mpiexec -n 2 python3 main.py 
echo "\nMPI = 3"
import -window root -resize 1600x900 -delay 5000 /home/sopel/Pictures/screenshotMPI_3.png
mpiexec -n 3 python3 main.py 
mpiexec -n 3 python3 main.py 
mpiexec -n 3 python3 main.py 
echo "\nMPI = 4"
import -window root -resize 1600x900 -delay 5000 /home/sopel/Pictures/screenshotMPI_4.png
mpiexec -n 4 python3 main.py 
mpiexec -n 4 python3 main.py 
mpiexec -n 4 python3 main.py 
echo "\nMPI = 5"
import -window root -resize 1600x900 -delay 5000 /home/sopel/Pictures/screenshotMPI_5.png
mpiexec -n 5 python3 main.py 
mpiexec -n 5 python3 main.py 
mpiexec -n 5 python3 main.py 
echo "\nMPI = 6"
import -window root -resize 1600x900 -delay 5000 /home/sopel/Pictures/screenshotMPI_6.png
mpiexec -n 6 python3 main.py 
mpiexec -n 6 python3 main.py 
mpiexec -n 6 python3 main.py 
