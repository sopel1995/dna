from mpi4py import MPI
import time
import math

DEBUG = False

def split_str(seq, chunk, skip_tail=False):
    lst = []
    if chunk <= len(seq):
        lst.extend([seq[:chunk]])
        lst.extend(split_str(seq[chunk:], chunk, skip_tail))
    elif not skip_tail and seq:
        lst.extend([seq])
    return lst
def compare_with_move(DNA, swifts):
    result = []
    for mov in swifts:
        DNA_mov = DNA[mov:]+DNA[0:mov]
        if (DEBUG): print(f"Compare: \n{DNA}\n{DNA_mov}")
        if (DEBUG): print(f"RESULT: {compare_string(DNA, DNA_mov)}")
        result += compare_string(DNA, DNA_mov)
    return  result
def compare_string(DNA, DNA_MOVE):
    index = [0 for x in range(len(DNA))]
    i = 0
    for x in range(0, len(DNA)):
        i += 1
        if(DNA[x] == DNA_MOVE[x]):
            index[x] = i
        else:
            i = 0
    index_max = max(index)
    if(index_max != 0 and index_max != 1):
        #print(index)
        #print(index_max)
        index_max_many = [i for i, x in enumerate(index) if x == index_max]
        #print(index_max_many)
        result = []
        for index in index_max_many:
            result.append(DNA[index-index_max+1:index+1])
        return(result)
    else:
        return ""
def main():
    time_start = time.time()
    with open("ciag1.txt", 'rb') as f:
        txt = f.read()

    txt = str(txt)[0:20000]
    #txt = "012345678912789"

    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    if rank == 0:
        data = []
        len_data = len(txt)
        len_section = math.ceil(len_data/size)
        for i in range(0, math.ceil(len_data/(len_data/size))):
            if(i != 0):
                index_start = i * len_section
            else:
                index_start = 1     #No compare with yoursalfe
            index_end = index_start + len_section
            if(len_data<index_end):
                index_end = len_data
            data.append([txt, index_start, index_end])
    else:
        data = None


    data = comm.scatter(data, root=0)       #   Work in machine
    result = compare_with_move(data[0], range(data[1], data[2]))
    if (DEBUG): print(f'Rank:{rank} has result: {result}')
    newData = comm.gather(result, root=0)       # Return reasulT

    if rank == 0:
        if (DEBUG): print(f'master:{newData}')
        time_end = time.time()
        print(f"Time process: {time_end - time_start}")


if __name__ == '__main__':
    main()
